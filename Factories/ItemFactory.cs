﻿using System;

namespace RpgHeroes
{
    public class ItemFactory
    {
        public static Weapon CreateWeapon(string name, int level, WeaponType weaponType)
        {
            Weapon weapon = new Weapon(name, level, weaponType);
            switch (weaponType)
            {
                case WeaponType.Melee:
                    weapon.BaseDamage = 15;
                    weapon.Damage = weapon.Level == 1 ? weapon.BaseDamage : weapon.BaseDamage - 2 + (weapon.Level * 2);
                    break;
                case WeaponType.Ranged:
                    weapon.BaseDamage = 5;
                    weapon.Damage = weapon.Level == 1 ? weapon.BaseDamage : weapon.BaseDamage - 3 + (weapon.Level * 3);
                    break;
                case WeaponType.Magic:
                    weapon.BaseDamage = 25;
                    weapon.Damage = weapon.Level == 1 ? weapon.BaseDamage : weapon.BaseDamage - 2 + (weapon.Level * 2);
                    break;
                default:
                    break;
            }
            return weapon;
        }

        //Calls on setstats to get the correct stats then returns the armor
        public static Armor CreateArmor(string name, int level, Material material, ArmorType armorType)
        {
            Armor armor = new Armor(name, level, material, armorType);
            switch (armorType)
            {
                case ArmorType.BodyArmor:
                    SetStats(armor, 1);
                    break;
                case ArmorType.HeadArmor:
                    SetStats(armor, 0.8);
                    break;
                case ArmorType.LegArmor:
                    SetStats(armor, 0.6);
                    break;
                default:
                    throw new NotSupportedException();
            }
            return armor;
        }

        //This method sets the correct stats when creating armor based on material, level and armor type(armor slot)
        private static void SetStats(Armor armor, double slotScaling)
        {
            double bonusHealth;
            double bonusStrength;
            double bonusDexterity;
            double bonusIntelligence;

            switch (armor.Material)
            {
                case Material.Plate:
                    bonusHealth = armor.Level == 1 ? 30 * slotScaling : (30 - 12 + (armor.Level * 12)) * slotScaling;
                    bonusStrength = armor.Level == 1 ? 3 * slotScaling : (3 - 2 + (armor.Level * 2)) * slotScaling;

                    armor.BonusHealth = (int)(bonusHealth);
                    armor.BonusStrength = (int)(bonusStrength);
                    armor.BonusDexterity = (int)(armor.Level * slotScaling);
                    break;
                case Material.Leather:
                    bonusHealth = armor.Level == 1 ? 20 * slotScaling : (30 - 8 + (armor.Level * 8)) * slotScaling;
                    bonusDexterity = armor.Level == 1 ? 3 * slotScaling : (3 - 2 + (armor.Level * 2)) * slotScaling;

                    armor.BonusHealth = (int)(bonusHealth);
                    armor.BonusStrength = (int)(armor.Level * slotScaling);
                    armor.BonusDexterity = (int)(bonusDexterity);
                    break;
                case Material.Cloth:
                    bonusHealth = armor.Level == 1 ? 10 * slotScaling : (10 - 5 + (armor.Level * 5)) * slotScaling;
                    bonusIntelligence = armor.Level == 1 ? 3 * slotScaling : (3 - 2 + (armor.Level * 2)) * slotScaling;

                    armor.BonusHealth = (int)(bonusHealth);
                    armor.BonusDexterity = (int)(armor.Level * slotScaling);
                    armor.BonusIntelligence = (int)(bonusIntelligence);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}