﻿using System;

namespace RpgHeroes
{
    class HeroFactory
    {
        //This factory extracts the creation of the hero out of the class
        public static Hero CreateHero(string name, HeroClass heroClass)
        {
            switch (heroClass)
            {
                case HeroClass.Warrior:
                    Hero warrior = new Warrior(name)
                    {
                        BaseHealth = 150,
                        BaseStrength = 10,
                        BaseDexterity = 3,
                        BaseIntelligence = 1
                    };
                    return warrior;
                case HeroClass.Mage:
                    Hero mage = new Mage(name)
                    {
                        BaseHealth = 100,
                        BaseStrength = 2,
                        BaseDexterity = 3,
                        BaseIntelligence = 10
                    };
                    mage.CalculateStats();
                    return mage;
                case HeroClass.Ranger:
                    Hero ranger = new Ranger(name)
                    {
                        BaseHealth = 120,
                        BaseStrength = 5,
                        BaseDexterity = 10,
                        BaseIntelligence = 2
                    };
                    return ranger;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
