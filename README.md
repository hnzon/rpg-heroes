﻿# Rpg Heroes

Rpg Heroes is a console game where you can create Heroes, attack monsters and find and equip gear for your hero.

## How to start the game

Clone the project. Open it and run from Visual Studio.

## Gameplay

First choose a name and class for your hero.

Then choose an option from the menu to perform an action.

Press help at any time to see all available options.

## Features

- Create a character
- Search the world for better gear
- Improve your stats by equipping gear
- Attack to gain experience
- Level up your character to improve stats and be able to equip more powerful gear


## Structure

The Project features a lot of inheritance but never more than parent/child relationships. 

I originally used interfaces for "Armor" and "Hero" but with the way the structure turned out I found it better to use only inheritance.

All items and heroes are created using factories which does all teh creation of base properties.


