﻿namespace RpgHeroes
{
    //All enums are stored in this class
    public enum WeaponType
    {
        Melee,
        Ranged,
        Magic
    }

    public enum HeroClass
    {
        Warrior,
        Mage,
        Ranger
    }

    public enum ArmorType
    {
        HeadArmor,
        BodyArmor,
        LegArmor
    }

    public enum Material
    {
        Plate,
        Leather,
        Cloth
    }
}
