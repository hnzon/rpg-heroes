﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RpgHeroes
{
    public class UserInteface
    {
        public Hero Player { get; set; }

        //This starts the game and then starts a while loop that always listens for input
        public void Start()
        {
            Console.WriteLine("Welcome to the world of RPG Heroes.");
            ShowPlayerHeroCreationMenu();
            Console.WriteLine();
            World.CreateWorld();
            ShowHelpMenu();
            Console.WriteLine();
            while (true)
            {
                Console.Write(">");
                string input = Console.ReadLine();
                string cleanInput = input.ToLowerInvariant();
                HandleInput(cleanInput);
                Console.WriteLine();
            }
        }

        //The menu which creates a hero for the user after calling ChooseHeroClass method
        public void ShowPlayerHeroCreationMenu()
        {
            Console.WriteLine("What is your name?");
            Console.WriteLine();
            Console.Write(">");
            string playerName = Console.ReadLine();
            Console.WriteLine();
            HeroClass playerClass = ChooseHeroClass();
            Player = HeroFactory.CreateHero(playerName, playerClass);
            Console.WriteLine();
            Console.WriteLine($"Welcome to the RPG world, {playerName}! And good luck");
        }

        private HeroClass ChooseHeroClass()
        {
            bool classChosen = false;
            while (!classChosen)
            {
                Console.WriteLine("What class of hero are you? Please choose a number:");
                Console.WriteLine("1. Warrior   2. Mage   3. Ranger");
                Console.WriteLine();
                Console.Write(">");
                HeroClass playerClass;
                string playerClassInput = Console.ReadLine();
                classChosen = true;
                switch (playerClassInput.ToLowerInvariant())
                {
                    case "1":
                    case "warrior":
                        playerClass = HeroClass.Warrior;
                        return playerClass;
                    case "2":
                    case "mage":
                        playerClass = HeroClass.Mage;
                        return playerClass;
                    case "3":
                    case "ranger":
                        playerClass = HeroClass.Ranger;
                        return playerClass;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Please choose one of the options");
                        Console.WriteLine();
                        classChosen = false;
                        break;
                }
            }
            throw new InvalidOperationException();
        }

        //The menu of all available inputs
        public void ShowHelpMenu()
        {
            Console.WriteLine("Here is the List of actions you can do:");
            Console.WriteLine("help: Show this menu");
            Console.WriteLine("search: Search for valuable items for your hero");
            Console.WriteLine("attack: Attack a monster");
            Console.WriteLine("stats: Show stats of your hero");
            Console.WriteLine("equipment: Show your current equipment and their stats");
            Console.WriteLine("stats weapon: Show stats of your weapon");
            Console.WriteLine("stats head: Show stats of your head armor");
            Console.WriteLine("stats body: Show stats of your body armor");
            Console.WriteLine("stats legs: Show stats of your leg armor");
            Console.WriteLine("exit: exit game");
        }


        private void HandleInput(string input)
        {
            Console.WriteLine();
            switch (input)
            {
                case "exit":
                case "quit":
                    Environment.Exit(0);
                    break;
                case "help":
                case "menu":
                    ShowHelpMenu();
                    break;
                case "search":
                    SearchForItems();
                    break;
                case "attack":
                    Player.Attack();
                    break;
                case "stats":
                    Player.GetDetails();
                    break;
                case "equipment":
                    Player.GetEquipment();
                    break;
                case "stats weapon":
                    Player.WeaponSlot.GetDetails();
                    break;
                case "stats head":
                    Player.HeadSlot.GetDetails();
                    break;
                case "stats body":
                    Player.BodySlot.GetDetails();
                    break;
                case "stats legs":
                    Player.LegSlot.GetDetails();
                    break;
                default:
                    //if input is invalid, shows menu again
                    Console.WriteLine("Please choose a supported action");
                    ShowHelpMenu();
                    break;
            }
        }

        //This calls on players "SearchForItems" method which searches for items in the "world" and returns 5 items which can be chosen to equip
        private void SearchForItems()
        {
            List<Item> foundItems = Player.SearchForItems();
            Console.WriteLine();

            Console.WriteLine("Choose the number of one of the items to equip it if your level is high enough");
            Console.WriteLine("back: go back");
            Console.WriteLine("search: search again for other items");
            Console.WriteLine();
            Console.Write(">");
            string input = Console.ReadLine();

            switch (input)
            {
                case "back":
                    Console.WriteLine("You leave the items behind");
                    break;
                case "search":
                    SearchForItems();
                    break;
                default:
                    bool inputIsNumber = Int32.TryParse(input, out int number);
                    if (inputIsNumber && Enumerable.Range(1, 5).Contains(number))
                    {
                        Console.WriteLine();
                        Player.Equip(foundItems[number - 1]);
                    }
                    else
                    {
                        Console.WriteLine("Please enter a number of one of the items");
                        Console.WriteLine("You lost those items and have to find some new ones");
                        SearchForItems();
                    }
                    break;
            }
        }
    }
}
