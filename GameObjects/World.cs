﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RpgHeroes
{
    //This is the world class. It is the world the Hero walks in and contains all items.
    //With more time I would expand this class to include monsters, locations etc and to be able to interact with them
    public static class World
    {
        public static List<Item> Items = new List<Item>();

        public static void CreateWorld()
        {
            PopulateItems();
        }

        public static void PopulateItems()
        {
            Items.Add(ItemFactory.CreateArmor("Old Linen Hat", 1, Material.Cloth, ArmorType.HeadArmor));
            Items.Add(ItemFactory.CreateArmor("Broken Helmet", 1, Material.Plate, ArmorType.HeadArmor));
            Items.Add(ItemFactory.CreateArmor("Flimsy Leather Hat", 1, Material.Leather, ArmorType.HeadArmor));
            Items.Add(ItemFactory.CreateArmor("Broken Plate Leggings", 1, Material.Plate, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Dirty Leather Pants", 1, Material.Leather, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Old Trousers", 1, Material.Cloth, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Squire's Trousers", 1, Material.Cloth, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Broken Iron Chestplate", 1, Material.Plate, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Old Leather Jacket", 1, Material.Leather, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Squire's Robe", 1, Material.Cloth, ArmorType.LegArmor));

            Items.Add(ItemFactory.CreateArmor("Novice Robe", 3, Material.Cloth, ArmorType.BodyArmor));
            Items.Add(ItemFactory.CreateArmor("Hunter's Vest", 4, Material.Leather, ArmorType.BodyArmor));
            Items.Add(ItemFactory.CreateArmor("Soldier's Chest Armor", 5, Material.Plate, ArmorType.BodyArmor));

            Items.Add(ItemFactory.CreateArmor("Soldier's Plate Legs", 3, Material.Plate, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Hunter's Leather Pants", 4, Material.Leather, ArmorType.LegArmor));
            Items.Add(ItemFactory.CreateArmor("Magician Silk Pants", 5, Material.Plate, ArmorType.LegArmor));

            Items.Add(ItemFactory.CreateArmor("Soldier's Helmet", 6, Material.Plate, ArmorType.HeadArmor));
            Items.Add(ItemFactory.CreateArmor("Hunter's Hat", 4, Material.Leather, ArmorType.HeadArmor));
            Items.Add(ItemFactory.CreateArmor("Magic Hat", 7, Material.Cloth, ArmorType.HeadArmor));
            Items.Add(ItemFactory.CreateArmor("Helmet of Justice", 11, Material.Plate, ArmorType.HeadArmor));

            Items.Add(ItemFactory.CreateWeapon("Magic Stick", 1, WeaponType.Magic));
            Items.Add(ItemFactory.CreateWeapon("Wizard Staff", 6, WeaponType.Magic));
            Items.Add(ItemFactory.CreateWeapon("Apprentice Staff", 3, WeaponType.Magic));
            Items.Add(ItemFactory.CreateWeapon("Hammer of Strength", 3, WeaponType.Melee));
            Items.Add(ItemFactory.CreateWeapon("Dirk", 1, WeaponType.Melee));
            Items.Add(ItemFactory.CreateWeapon("Longsword", 7, WeaponType.Melee));
            Items.Add(ItemFactory.CreateWeapon("Slingshot", 1, WeaponType.Ranged));
            Items.Add(ItemFactory.CreateWeapon("Crossbow", 8, WeaponType.Ranged));
            Items.Add(ItemFactory.CreateWeapon("Flimsy bow", 3, WeaponType.Ranged));

            Items.Add(ItemFactory.CreateWeapon("Sword of a Thousand Truths", 60, WeaponType.Melee));
        }

        //This is where the hero finds the items after searching
        //It returns 5 items as it would be too much text on the screen with more items
        public static List<Item> GetFoundItems()
        {
            Random rnd = new Random();
            List<Item> foundItems = Items.OrderBy(x => rnd.Next()).Take(5).ToList();
            return foundItems;
        }
    }

}

