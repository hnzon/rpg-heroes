﻿namespace RpgHeroes
{
    //Since all items have a name and level, all items inherit from this abstract class
    public abstract class Item
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public Item(string name, int level)
        {
            Name = name;
            Level = level;
        }

        public abstract void GetDetails();

        //This is a more shorthand version to not get too much info in the console when searching for items
        public abstract void GetSimpleDetails();
    }
}