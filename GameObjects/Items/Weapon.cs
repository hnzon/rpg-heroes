﻿using System;

namespace RpgHeroes
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public int BaseDamage { get; set; }
        public int Damage { get; set; }

        public Weapon(string name, int level, WeaponType weaponType) : base(name, level)
        {
            WeaponType = weaponType;
        }

        public override void GetDetails()
        {
            Console.WriteLine("Weapon Details:");
            Console.WriteLine(Name);
            Console.WriteLine($"Level {Level}");
            Console.WriteLine($"Weapon Type {WeaponType}");
            Console.WriteLine($"Damage: {Damage}");
        }

        public override void GetSimpleDetails()
        {
            Console.WriteLine($"\t{Name}");
            Console.WriteLine($"\tLevel {Level} {WeaponType} Weapon");
            Console.WriteLine($"\tDamage: {Damage}");
        }
    }
}