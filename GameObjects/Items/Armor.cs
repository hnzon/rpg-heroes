﻿using System;

namespace RpgHeroes
{
    public class Armor : Item
    {
        public int BonusHealth { get; set; }
        public int BonusStrength { get; set; }
        public int BonusDexterity { get; set; }
        public int BonusIntelligence { get; set; }
        public Material Material { get; set; }
        public ArmorType ArmorType { get; set; }

        public Armor(string name, int level, Material material, ArmorType armorType) : base(name, level)
        {
            Material = material;
            ArmorType = armorType;
        }

        public override void GetDetails()
        {
            Console.WriteLine(Name);
            Console.WriteLine($"Level {Level}");
            Console.WriteLine($"Armor Type: {ArmorType}");
            Console.WriteLine($"Material: {Material}");
            Console.WriteLine($"HP: {BonusHealth}");
            Console.WriteLine($"Strength: {BonusStrength}");
            Console.WriteLine($"Dexterity: {BonusDexterity}");
            Console.WriteLine($"Intelligence: {BonusIntelligence}");
        }

        public override void GetSimpleDetails()
        {
            Console.WriteLine($"\t{Name}");
            Console.WriteLine($"\tLevel {Level} {Material} {ArmorType}");
            Console.WriteLine($"\tHealth: {BonusHealth} - Strength: {BonusStrength} - Intelligence: {BonusIntelligence} - Dexterity: {BonusDexterity}");
        }
    }
}