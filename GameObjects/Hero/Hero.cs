﻿using System;
using System.Collections.Generic;

namespace RpgHeroes
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int BaseHealth { get; set; }
        public int BaseStrength { get; set; }
        public int BaseDexterity { get; set; }
        public int BaseIntelligence { get; set; }
        public int BonusHealth { get; set; }
        public int BonusStrength { get; set; }
        public int BonusDexterity { get; set; }
        public int BonusIntelligence { get; set; }
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public ItemSlot WeaponSlot { get; set; }
        public ItemSlot HeadSlot { get; set; }
        public ItemSlot BodySlot { get; set; }
        public ItemSlot LegSlot { get; set; }

        public Hero(string name)
        {
            Name = name;
            Level = 1;
            Experience = 0;
            WeaponSlot = new ItemSlot();
            HeadSlot = new ItemSlot();
            BodySlot = new ItemSlot();
            LegSlot = new ItemSlot();
        }

        public void CalculateStats()
        {
            Health = BaseHealth + BonusHealth;
            Intelligence = BaseIntelligence + BonusIntelligence;
            Strength = BaseStrength + BonusStrength;
            Dexterity = BaseDexterity + BonusDexterity;
        }

        //This method first calls unequip method if it's armor which just subtracts the current item's stats from the hero's stats
        //it then calls on the corresponding itemslot of the type of armor/weapon to equip it
        public void Equip(Item item)
        {
            if (item.Level > Level)
            {
                Console.WriteLine("Your level is too low to equip this item");
                return;
            }
            if (item is Weapon weapon)
            {
                WeaponSlot.Equip(weapon);
            }
            else if (item is Armor armorItem)
            {
                switch (armorItem.ArmorType)
                {
                    case ArmorType.HeadArmor:
                        Item currentHeadArmor = HeadSlot.EquippedItem;
                        if (currentHeadArmor != null)
                        {
                            UnEquip((Armor)currentHeadArmor);
                        }
                        HeadSlot.Equip(armorItem);
                        break;
                    case ArmorType.BodyArmor:
                        Item currentBodyArmor = HeadSlot.EquippedItem;
                        if (currentBodyArmor != null)
                        {
                            UnEquip((Armor)currentBodyArmor);
                        }
                        BodySlot.Equip(armorItem);
                        break;
                    case ArmorType.LegArmor:
                        Item currentLegArmor = HeadSlot.EquippedItem;
                        if (currentLegArmor != null)
                        {
                            UnEquip((Armor)currentLegArmor);
                        }
                        LegSlot.Equip(armorItem);
                        break;
                    default:
                        throw new NotSupportedException();
                }
                BonusHealth += armorItem.BonusHealth;
                BonusStrength += armorItem.BonusStrength;
                BonusIntelligence += armorItem.BonusIntelligence;
                BonusDexterity += armorItem.BonusDexterity;
                CalculateStats();
            }
            Console.WriteLine($"Equipped {item.Name}");
        }

        //This subtracts the stats, the uneuip method in the itemslot class does the actual removal of the item from the slot
        public void UnEquip(Armor armorItem)
        {
            BonusHealth -= armorItem.BonusHealth;
            BonusStrength -= armorItem.BonusStrength;
            BonusIntelligence -= armorItem.BonusIntelligence;
            BonusDexterity -= armorItem.BonusDexterity;
            CalculateStats();
        }

        //This method first gets the calculatedd damage then gets 50 experience, which was a random number I chose to get for each attack
        //It then checks in "LevelService" if it has enough xp to level up in which case it call level up method
        public void Attack()
        {
            int damage = CalculateDamage();
            int experienceGained = 50;
            Console.WriteLine($"You attacked for {damage} damage");
            Console.WriteLine($"You gained {experienceGained} experience");
            Experience += experienceGained;
            Console.WriteLine($"Total xp: {Experience} experience");
            GetRemainingXP();
            if (LevelService.EnoughXpForLevelUp(Level, Experience))
            {
                LevelUp();
            }
        }

        //In this method, the hero searches the world for items and gets 5 random ones
        public List<Item> SearchForItems()
        {
            List<Item> foundItems = World.GetFoundItems();

            Console.WriteLine("You go searching around the area and you find the following items:");
            Console.WriteLine();

            for (int i = 0; i < foundItems.Count; i++)
            {
                Console.Write($"{i + 1}: ");
                foundItems[i].GetSimpleDetails();
                Console.WriteLine();
            }
            return foundItems;

        }

        //Returns the correct damage output to the attack method
        public int CalculateDamage()
        {
            int damage = 0;
            Weapon weapon = (Weapon)WeaponSlot.EquippedItem;

            if (weapon == null)
            {
                damage = 0;
            }
            else
            {
                switch (weapon.WeaponType)
                {
                    case WeaponType.Melee:
                        damage = (int)((Strength + weapon.Damage) * 1.5);
                        break;
                    case WeaponType.Ranged:
                        damage = (Dexterity + weapon.Damage) * 2;
                        break;
                    case WeaponType.Magic:
                        damage = (Intelligence + weapon.Damage) * 3;
                        break;
                    default:
                        break;
                }
            }
            return damage;
        }

        public void LevelUp()
        {
            Level++;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine("DING");
            Console.ResetColor();
            Console.WriteLine($"Congratulations! You leveled up. You are now level {Level}");
            ClassLevelUp();
        }

        //All classes have their own increase of stats so they all implement their own version of this method
        protected abstract void ClassLevelUp();

        private void GetRemainingXP()
        {
            //If this gets called from attack method and you already leveled up it shows "0 xp to next level" instead of a negative number
            int remainingXp = LevelService.GetRemainingXp(Level, Experience) > 0 ? LevelService.GetRemainingXp(Level, Experience) : 0;
            Console.WriteLine($"XP to next level: {remainingXp}");
        }

        public void GetDetails()
        {
            Console.WriteLine("Hero Details:");
            Console.WriteLine($"Name {Name}");
            Console.WriteLine($"Level {Level} {this.GetType().Name}");
            Console.WriteLine($"HP: {Health}");
            Console.WriteLine($"Strength: {Strength}");
            Console.WriteLine($"Dexterity: {Dexterity}");
            Console.WriteLine($"Intelligence: {Intelligence}");
            GetRemainingXP();
        }

        public void GetEquipment()
        {
            Console.WriteLine("WEAPON:");
            WeaponSlot.GetDetails();
            Console.WriteLine();
            Console.WriteLine("HEAD:");
            HeadSlot.GetDetails();
            Console.WriteLine();
            Console.WriteLine("BODY:");
            BodySlot.GetDetails();
            Console.WriteLine();
            Console.WriteLine("LEGS:");
            LegSlot.GetDetails();
        }
    }
}
