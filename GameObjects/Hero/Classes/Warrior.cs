﻿namespace RpgHeroes
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
        }

        protected override void ClassLevelUp()
        {
            BaseHealth += 30;
            BaseStrength += 5;
            BaseDexterity += 2;
            BaseIntelligence += 1;
            CalculateStats();
        }
    }
}
