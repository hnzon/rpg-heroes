﻿namespace RpgHeroes
{
    public class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {
        }

        protected override void ClassLevelUp()
        {
            BaseHealth += 20;
            BaseStrength += 2;
            BaseDexterity += 5;
            BaseIntelligence += 1;
            CalculateStats();
        }
    }
}
