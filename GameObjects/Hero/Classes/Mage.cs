﻿namespace RpgHeroes
{
    public class Mage : Hero
    {
        public Mage(string name) : base(name)
        {
        }

        protected override void ClassLevelUp()
        {
            BaseHealth += 15;
            BaseStrength += 1;
            BaseDexterity += 2;
            BaseIntelligence += 5;
            CalculateStats();
        }
    }
}
