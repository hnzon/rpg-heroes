﻿using System;

namespace RpgHeroes

{
    //This is a seperate class for the ItemSlot to make equipping and getting item details from hero more clear and efficient
    public class ItemSlot
    {
        //Nullable is enabled since EquippedItem should be allowed to be null if unequipped
#nullable enable
        public Item? EquippedItem { get; set; }
#nullable disable

        public ItemSlot()
        {
            EquippedItem = null;
        }

        public void Equip(Item item)
        {
            if (EquippedItem != null)
            {
                Unequip();
            }
            EquippedItem = item;
        }

        public void Unequip()
        {
            Console.WriteLine($"Unequipped {EquippedItem.Name}");
            EquippedItem = null;
        }

        public void GetDetails()
        {
            if (EquippedItem != null)
            {
                EquippedItem.GetDetails();
            }
            else
            {
                Console.WriteLine("You have no item equipped in this slot");
            }
        }
    }
}
