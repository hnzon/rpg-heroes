﻿using System;

namespace RpgHeroes
{
    public static class LevelService
    {
        public static bool EnoughXpForLevelUp(int level, int experience)
        {
            int requiredXp = (int)((level * 100) * Math.Pow(1.1, level - 1));
            return experience >= requiredXp;
        }

        public static int GetRemainingXp(int level, int experience)
        {
            int requiredXp = (int)((level * 100) * Math.Pow(1.1, level - 1));
            return requiredXp - experience;
        }
    }
}
